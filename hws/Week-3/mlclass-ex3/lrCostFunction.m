function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y);     % number of training examples
n = length(theta); % number of features

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

Z = zeros(size(theta));
Z = X*theta;

h = zeros(size(theta));
h = sigmoid(Z);

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta


%compute the cost function - vectorized!
J = -(y'*log(h) +(1.0 - y)'*log(1.0 - h)) + lambda*theta'*theta/2;
J = J - lambda*theta(1,1)*theta(1,1)/2;
J = J/m;

%compute the gradient - vectorized!
grad = X'*(h - y) + lambda*theta;
grad(1,1) = grad(1,1) - lambda*theta(1,1);
grad = grad/m;
end
