function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y);     % number of training examples
n = length(theta); % number of features

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% Compute z = X*theta
Z = zeros(m, 1);
Z = X*theta;

%Compute the logmid function for all members of the tarining set
h = zeros(m,1);
h = sigmoid(Z);

%compute the cost function
J = -(y'*log(h) +(1-y)'*log(1-h)) + theta'*theta/2;
J = J - theta(1,1)*theta(1,1)/2;
J = J/m;

%compute the gradient
grad = X'*(y - h) + lambda*theta;
grad(1,1) = grad(1,1) - lambda*theta(1,1);
grad = grad/m;


% =============================================================

%grad = grad(:);

%end
