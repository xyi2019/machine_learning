function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%
N1 = input_layer_size;
N2 = hidden_layer_size;
K = num_labels;
fprintf('n1 = %d, n2 = %d, K = %d \n', N1, N2, K);

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));
[m1, n1] = size(Theta1);
fprintf('m1 = %d, n1 = %d \n', m1, n1);

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));
[m2, n2] = size(Theta2);
fprintf('m2 = %d, n2 = %d \n', m2, n2);

% Setup some useful variables
m = size(X, 1);
fprintf('m = %d\n', m);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

%{
%Form y-matrix
[my, ny] = size(y);
fprintf('my = %d, ny = %d \n', my, ny);
Y = zeros(m, K);
for i = 1:m
    d = y(i,1);
    Y(i, d) = 1;    
end

% Add bias into X
X = [ones(m,1) X];
a_1 = X;

% Mapping from the inoput layer to hidden layer
Z_2 = a_1*Theta1';
a_2 = sigmoid(Z_2);

[ma_2, na_2] = size(a_2);
fprintf('ma_2 = %d, na_2 = %d \n', ma_2, na_2);

% Add bias into a_2
a_2 = [ones(m,1) a_2];

% Mapping from the hidden layer to output layer
Z_3 = a_2*Theta2';
a_3 = sigmoid(Z_3);

[ma_3, na_3] = size(a_3);
fprintf('ma_3 = %d, na_3 = %d \n', ma_3, na_3);

%Compute the cost function - vectorized!
%{
J = -(y'*log(a_2) +(1.0 - y)'*log(1.0 - a_2)) + lambda*theta2'*theta2/2;
J = J - lambda*theta2(1,1)*theta2(1,1)/2;
J = J/m;
%}

%Compute the cost function
J = 0;
for i = 1:m
    for j = 1:K
        J = J + Y(i,j)*log(a_3(i,j)) + (1 - Y(i,j))*log(1.0 - a_3(i,j));
    end
end
J = -J;

% Add the regularizatiion terms
R1 = 0;
for i = 1:m1
    for j = 2:n1
        R1 = R1 + Theta1(i,j)*Theta1(i,j);
    end
end

R2 = 0;
for i = 1:m2
    for j = 2:n2
        R2 = R2 + Theta2(i,j)*Theta2(i,j);
    end
end

J = J + lambda*R1/2;
J = J + lambda*R2/2;

J = J/m;
%}

% -------------------------------------------------------------
[my, ny] = size(y);
fprintf('my = %d, ny = %d \n', my, ny);

% Add bias into X
X = [ones(m,1) X];

%Loop over all samples 
J = 0;
for i = 1:m
    
    %Extract the i-th sample from the ith row of X
    x_i = X(i,:);  
    a_1 = x_i';
    
    %{
    [ma_1, na_1] = size(a_1);
    fprintf('ma_1 = %d, na_1 = %d \n', ma_1, na_1);
    %}
    
    % Extract the y-vector for the i-th sample
    yi = zeros(K, 1);
    di = y(i,1);
    yi(di, 1) = 1;

    % Mapping from the inoput layer to hidden layer
    Z_2 = Theta1*a_1;
    a_2 = sigmoid(Z_2);
        
    % Add bias into a_2
    a_2 = [1; a_2];
    
    %{
    [ma_2, na_2] = size(a_2);
    fprintf('ma_2 = %d, na_2 = %d \n', ma_2, na_2);
    %}

    % Mapping from the hidden layer to output layer
    Z_3 = Theta2*a_2;
    a_3 = sigmoid(Z_3);
    
    %{
    [ma_3, na_3] = size(a_3);
    fprintf('ma_3 = %d, na_3 = %d \n', ma_3, na_3);
    %}
    
    %Accumulate the cost function    
    for k = 1:K
        J = J + yi(k,1)*log(a_3(k,1)) + (1 - yi(k,1))*log(1.0 - a_3(k,1));
    end
    
    %Compute delta's
    delta_3 = a_3 - yi;
    delta_2 = (Theta2'*delta_3).*(a_2.*(1.0 - a_2));
    delta_2 = delta_2(2:end);
    
    if i == 1
        Delta_1 = delta_2*a_1';
        Delta_2 = delta_3*a_2';
    else
        Delta_1 = Delta_1 + delta_2*a_1';
        Delta_2 = Delta_2 + delta_3*a_2';
    end
end
    
J = -J/m;
Theta1_grad = Delta_1/m;
Theta2_grad = Delta_2/m;

% Add the regularizatiion terms
R1 = 0;
for i = 1:m1
    for j = 2:n1
        R1 = R1 + Theta1(i,j)*Theta1(i,j);
    end
end

R2 = 0;
for i = 1:m2
    for j = 2:n2
        R2 = R2 + Theta2(i,j)*Theta2(i,j);
    end
end

J = J + lambda*R1/(2*m);
J = J + lambda*R2/(2*m);

Theta1_grad(:,2:end) = Theta1_grad(:,2:end) + lambda*Theta1(:,2:end)/m;
Theta2_grad(:,2:end) = Theta2_grad(:,2:end) + lambda*Theta2(:,2:end)/m;

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];

end
