function [J, grad] = cofiCostFunc(params, Y, R, num_users, num_movies, ...
                                  num_features, lambda)
%COFICOSTFUNC Collaborative filtering cost function
%   [J, grad] = COFICOSTFUNC(params, Y, R, num_users, num_movies, ...
%   num_features, lambda) returns the cost and gradient for the
%   collaborative filtering problem.
%

nu = num_users;       % index i  - movies
nm = num_movies;      % index j  - users
n  = num_features;

% Unfold the U and W matrices from params
X = reshape(params(1:num_movies*num_features), num_movies, num_features);
Theta = reshape(params(num_movies*num_features+1:end), ...
                num_users, num_features);

            
% You need to return the following values correctly
J = 0;
X_grad = zeros(size(X));
Theta_grad = zeros(size(Theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost function and gradient for collaborative
%               filtering. Concretely, you should first implement the cost
%               function (without regularization) and make sure it is
%               matches our costs. After that, you should implement the 
%               gradient and use the checkCostFunction routine to check
%               that the gradient is correct. Finally, you should implement
%               regularization.
%
% Notes: X - num_movies  x num_features matrix of movie features
%        Theta - num_users  x num_features matrix of user features
%        Y - num_movies x num_users matrix of user ratings of movies
%        R - num_movies x num_users matrix, where R(i, j) = 1 if the 
%            i-th movie was rated by the j-th user
%
% You should set the following variables correctly:
%
%        X_grad - num_movies x num_features matrix, containing the 
%                 partial derivatives w.r.t. to each element of X
%        Theta_grad - num_users x num_features matrix, containing the 
%                     partial derivatives w.r.t. to each element of Theta
%

for i = 1:nm
    for j = 1:nu
        if (R(i,j) == 1)
            J = J + (X(i,:)*Theta(j,:)' - Y(i,j))^2;            
            for k = 1:n                
                X_grad(i, k) = X_grad(i, k) + (X(i,:)*Theta(j,:)' - Y(i,j))*Theta(j,k);
                Theta_grad(j, k) = Theta_grad(j, k) +(X(i,:)*Theta(j,:)' - Y(i,j))*X(i,k);
            end
        end
    end % loop for j - users    
end % loop for i -- movies

% Add the regurization terms forthe cost function
for k = 1:n
    for i = 1:nm
        J = J + lambda*X(i,k)^2;  
    end
    
    for j = 1:nu
        J = J + lambda*Theta(j,k)^2;  
    end
end

% Add the regurization terms for the cost function gradient to X(i,k),
% Theta(j,k)
X_grad = X_grad + lambda*X;
Theta_grad = Theta_grad + lambda*Theta;

%{
% Vectorized version finally work! Pre-allocation seems not required
D = X*Theta' - Y;
E = D .* R;
E2 = E .^ 2;
J = sum(E2(:));
J = J/2;
%}

% =============================================================

grad = [X_grad(:); Theta_grad(:)];

end
