function idx = findClosestCentroids(X, centroids)
%FINDCLOSESTCENTROIDS computes the centroid memberships for every example
%   idx = FINDCLOSESTCENTROIDS (X, centroids) returns the closest centroids
%   in idx for a dataset X where each row is a single example. idx = m x 1 
%   vector of centroid assignments (i.e. each entry in range [1..K])
%

% Set K
K = size(centroids, 1);
l = size(centroids, 2);
%fprintf(' K = %d l = %d \n', K, l);

% m is the size of training sets, n is the dimension of each sample -
% so each row is a data sample I guess
[m ,n] = size(X);
%fprintf(' m = %d n = %d \n', m, n);

% You need to return the following variables correctly.
idx = zeros(size(X,1), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Go over every example, find its closest centroid, and store
%               the index inside idx at the appropriate location.
%               Concretely, idx(i) should contain the index of the centroid
%               closest to example i. Hence, it should be a value in the 
%               range 1..K
%
% Note: You can use a for-loop over the examples to compute this.
%
xi = zeros(1, n);
ck = zeros(1, n)
for i = 1:m % loop through all smaples
    dmin = 1000000000;
    idi = -1;    
    xi = X(i,:) % assign ith row of X to xi
    for k = 1:K % loop through all centrolds
        ck = centroids(k,:); %assign kth row of centroids to ck
        
        dik = 0;
        for j = 1:n % loop through vector components
            dik =  dik + (xi(1,j) - ck(1,j))^2;
        end
        
        if (dik < dmin)
            idi = k;
            dmin = dik
        end
    end
    
    idx(i,1) = idi;            
    %fprintf(' assign centroid: i: %d c = %d \n', i, idi);
end





% =============================================================

end

