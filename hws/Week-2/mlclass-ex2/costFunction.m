function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
[mx,nx] = size(X);
fprintf('Dimesion of X: %d %d \n', mx, nx);

[my,ny] = size(y);
fprintf('Dimesion of y: %d %d \n', my, ny);

m = length(y); % number of training examples
fprintf('m = : %d \n', m);

% You need to return the following variables correctly 
J = 0;
grad  = zeros(size(theta));
delta = zeros(size(theta)); % h(x,theta) - y

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%

%Compute the cost function and gradient
for j = 1: m % Loop through the training set
    zj = X(j,:)*theta;
    hj = sigmoid(zj);
    J  = J + y(j,1)*log(hj) + (1-y(j,1))*log(1-hj);    
    delta(j,1) = hj - y(j,1);    
end
J = -J/m;
grad = X'*delta/m;

% =============================================================

end
