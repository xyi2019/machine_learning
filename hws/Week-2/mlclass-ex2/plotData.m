function plotData(X, y)
%PLOTDATA Plots the data points X and y into a new figure 
%   PLOTDATA(x,y) plots the data points with + for the positive examples
%   and o for the negative examples. X is assumed to be a Mx2 matrix.

% Create New Figure
figure; hold on;

% ====================== YOUR CODE HERE ======================
% Instructions: Plot the positive and negative examples on a
%               2D plot, using the option 'k+' for the positive
%               examples and 'ko' for the negative examples.
%
n = length(y);
m = 0;
for i = 1:n
    if (y(i,1) == 0)
        m = m + 1;
    end
end

fprintf('n = %d, m = %d \n', n, m);

x0 = zeros(m, 1);
y0 = zeros(m, 1);
x1 = zeros(n-m, 1);
y1 = zeros(n-m, 1);

k = 1; 
l = 1;
for i = 1:n
    if (y(i,1) == 0)
        x0(k,1) = X(i,1);
        y0(k,1) = X(i,2);
        k = k + 1;
    else
        x1(l,1) = X(i,1);
        y1(l,1) = X(i,2);
        l = l + 1;
    end
end
   
scatter(x0, y0,'ko');
scatter(x1, y1,'r+');








% =========================================================================



hold off;

end
