function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples
fprintf('m = %d \n', m);

[mx, nx] = size(X);
fprintf('mx = %d my = %d \n', mx, nx);

[mt, nt] = size(theta);
fprintf('mt = %d mt = %d \n', mt, nt);

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%

% Compute cost function
delta = zeros(size(y));
delta = X*theta - y;
J = delta'*delta + lambda*(theta'*theta - theta(1,1)*theta(1,1));
J = J/(2*m);

% Compute gradient
grad = X'*(X*theta - y) + lambda*theta;
grad(1,1)  =  grad(1,1) - lambda*theta(1,1);
grad = grad/m;

% =========================================================================

grad = grad(:);

end
